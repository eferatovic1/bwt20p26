const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('bwt26ST', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
});

const Aktivnost = require(__dirname + '/models/aktivnost.js')(sequelize)
const Dan = require(__dirname + '/models/dan.js')(sequelize)
const Grupa = require(__dirname + '/models/grupa.js')(sequelize)
const Predmet = require(__dirname + '/models/predmet.js')(sequelize)
const Student = require(__dirname + '/models/student.js')(sequelize)
const Tip = require(__dirname + '/models/tip.js')(sequelize)
const StudentGrupa = require(__dirname + '/models/studentGrupa.js')(sequelize)

Predmet.hasMany(Grupa)
Predmet.hasMany(Aktivnost);
Aktivnost.hasMany(Grupa);
Dan.hasMany(Aktivnost);
Tip.hasMany(Aktivnost);
Student.belongsToMany(Grupa, { through: StudentGrupa });

sequelize.sync().then(() => {
    console.log("Povezano na bazu!")
});

module.exports = {
    Aktivnost,
    Dan,
    Grupa,
    Predmet,
    Student,
    Tip,
    StudentGrupa
}