const Sequelize = require("sequelize");

module.exports = function (sequelize) {
    const Student = sequelize.define(
        "student",
        {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: true,
                primaryKey: true,
                autoIncrement: true,
            },
            ime: {
                type: Sequelize.STRING,
                allowNull: false
            },
            index: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
        },
        { freezeTableName: true, timestamps: false }
    );
    return Student;
};