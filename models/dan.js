const Sequelize = require("sequelize");

module.exports = function (sequelize) {
    const Dan = sequelize.define(
        "dan",
        {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: true,
                primaryKey: true,
                autoIncrement: true,
            },
            naziv: {
                type: Sequelize.STRING,
                allowNull: false,
            },
        },
        { freezeTableName: true, timestamps: false }
    );
    return Dan;
};