const Sequelize = require("sequelize");

module.exports = function (sequelize) {
    const StudentGrupa = sequelize.define(
        "studentGrupa",
        {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: true,
                primaryKey: true,
                autoIncrement: true,
            },
            studentId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            grupaId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
        },
        { freezeTableName: true, timestamps: false }
    );
    return StudentGrupa;
};