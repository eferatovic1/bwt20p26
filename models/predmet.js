const Sequelize = require("sequelize");

module.exports = function (sequelize) {
    const Predmet = sequelize.define(
        "predmet",
        {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: true,
                primaryKey: true,
                autoIncrement: true,
            },
            naziv: {
                type: Sequelize.STRING,
                allowNull: false,
            },
        },
        { freezeTableName: true, timestamps: false }
    );
    return Predmet;
};