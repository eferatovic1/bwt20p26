const Sequelize = require("sequelize");

module.exports = function (sequelize) {
    const Aktivnost = sequelize.define(
        "aktivnost",
        {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: true,
                primaryKey: true,
                autoIncrement: true,
            },
            naziv: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            pocetak: {
                type: Sequelize.FLOAT,
                allowNull: false,
            },
            kraj: {
                type: Sequelize.FLOAT,
                allowNull: false,
            },
        },
        { freezeTableName: true, timestamps: false }
    );
    return Aktivnost;
};