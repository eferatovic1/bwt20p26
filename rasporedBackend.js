const express = require('express');
const app = express();
const fs = require('fs');
const url = require('url');
const {
    Aktivnost,
    Dan,
    Grupa,
    Predmet,
    Student,
    Tip,
    StudentGrupa
} = require('./baza')
app.use(express.json());
app.use(express.static(__dirname + '/static'))

app.get('/v1/raspored', (req, res) => {
    fs.readFile(__dirname + '/static/raspored.csv', async (err, data) => {
        if (err) {
            res.status(500);
            res.send({
                'greska': 'Datoteka raspored.csv nije kreirana!'
            })
            res.end()
        } else {
            let niz = [];
            let dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak']
            let rasporedNiz = data.toString().split('\n');
            for (let i = 0; i < rasporedNiz.length; i++) {
                let p = rasporedNiz[i];
                let predmetNiz = p.split(',');
                let predmetCSV = {
                    naziv: predmetNiz[0].split(':')[1],
                    aktivnost: predmetNiz[1].split(':')[1],
                    dan: predmetNiz[2].split(':')[1],
                    pocetak: predmetNiz[3].split('vrijemePocetka:')[1],
                    kraj: predmetNiz[4].split('vrijemeKraja:')[1]
                }
                niz.push(predmetCSV);
            }
            if (req.query.dan) {
                if (req.query.dan != null && req.query.dan != '')
                    niz = niz.filter((element) => {
                        return element.dan == req.query.dan
                    })
            }
            if (req.query.sort) {
                let nacin = req.query.sort.slice(0, 1);
                let atribut = req.query.sort.slice(1);

                switch (atribut) {
                    case 'naziv': {
                        niz.sort((a, b) => {
                            if (a.naziv > b.naziv) return 1;
                            if (a.naziv < b.naziv) return -1
                            return 0;
                        });
                    }
                        break;
                    case 'aktivnost': {
                        niz.sort((a, b) => {
                            if (a.aktivnost > b.aktivnost) return 1;
                            if (a.aktivnost < b.aktivnost) return -1
                            return 0;
                        });
                    }
                        break;
                    case 'dan': {
                        niz.sort((a, b) => {
                            if (dani.indexOf(a.dan) + 1 > dani.indexOf(b.dan) + 1) return 1;
                            if (dani.indexOf(a.dan) + 1 < dani.indexOf(b.dan) + 1) return -1
                            return 0;
                        });
                    }
                        break;
                    case 'pocetak': {
                        niz.sort((a, b) => {
                            if (a.pocetak > b.pocetak) return 1;
                            if (a.pocetak < b.pocetak) return -1
                            return 0;
                        });
                    }
                        break;
                    case 'kraj': {
                        niz.sort((a, b) => {
                            if (a.kraj > b.kraj) return 1;
                            if (a.kraj < b.kraj) return -1
                            return 0;
                        });
                    }
                        break;
                }

                if (nacin == 'D') {
                    niz.reverse();
                }
            }
            if (req.get('Accept') === 'text/csv') {
                let csv = '';
                for (let i = 0; i < niz.length; i++) {
                    let predmet = niz[i];
                    let line = 'naziv:' + predmet.naziv + ',aktivnost:' + predmet.aktivnost + ',dan:' + predmet.dan + ',vrijemePocetka:' + predmet.pocetak + ',vrijemeKraja:' + predmet.kraj;
                    csv += line + '\n';
                }
                res.send(csv).end();
            } else {
                res.send(niz);
                res.end();
            }
        }
    })
})

app.post('/v1/raspored', async (req, res) => {
    let tijeloZahtjeva = '';
    await req.on('data', function (data) {
        tijeloZahtjeva += data;
    });
    req.on('end', () => {
        let parametri = Object.fromEntries(new url.URLSearchParams(tijeloZahtjeva));
        let predmet = {
            naziv: parametri['naziv'],
            aktivnost: parametri['aktivnost'],
            dan: parametri['dan'],
            vrijemePocetka: parametri['vrijemePocetka'],
            vrijemeKraja: parametri['vrijemeKraja']
        }
        //da li su svi parametri u objektu predmet validni (nisu null, undefined itd)
        let validno = true;
        Object.values(predmet).forEach(i => {
            if (!i) {
                validno = false
            }
        });

        if (validno) {
            fs.readFile(__dirname + '/static/raspored.csv', async (err, data) => {
                if (err) {
                    res.status(500);
                    res.send('Problem sa citanjem datoteke raspored.csv!')
                    res.end()
                } else {
                    let rasporedNiz = data.toString().split('\n');
                    let ubaci = true;
                    for (let i = 0; i < rasporedNiz.length; i++) {
                        let p = rasporedNiz[i];
                        let predmetNiz = p.split(',');
                        let predmetCSV = {
                            naziv: predmetNiz[0].split(':')[1],
                            aktivnost: predmetNiz[1].split(':')[1],
                            dan: predmetNiz[2].split(':')[1],
                            pocetak: predmetNiz[3].split('vrijemePocetka:')[1],
                            kraj: predmetNiz[4].split('vrijemeKraja:')[1]
                        }
                        //pretvorba iz jsona u dates
                        let vp = new Date(2000, 10, 10, predmet.vrijemePocetka.split(':')[0], predmet.vrijemePocetka.split(':')[1], 0, 0);
                        let vk = new Date(2000, 10, 10, predmet.vrijemeKraja.split(':')[0], predmet.vrijemeKraja.split(':')[1], 0, 0);
                        let vpCSV = new Date(2000, 10, 10, predmetCSV.pocetak.split(':')[0], predmetCSV.pocetak.split(':')[1], 0, 0);
                        let vkCSV = new Date(2000, 10, 10, predmetCSV.kraj.split(':')[0], predmetCSV.kraj.split(':')[1], 0, 0);

                        //ovdje se vrsi provjera za svaki predmet u rasporedu da li se preklapa sa novim predmetom
                        if (predmet.naziv === predmetCSV.naziv && predmet.aktivnost === predmetCSV.aktivnost) {
                            ubaci = false;
                            break;
                        } else {
                            if (predmet.dan != predmetCSV.dan) {
                                ubaci = true;
                            } else {
                                if (vp == vpCSV && vk == vkCSV) {
                                    ubaci = false;
                                    break;
                                }
                                if ((vk <= vpCSV || vp >= vkCSV)) {
                                    ubaci = true;
                                } else {
                                    ubaci = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (ubaci) {
                        let line = '\nnaziv:' + predmet.naziv + ',aktivnost:' + predmet.aktivnost + ',dan:' + predmet.dan + ',vrijemePocetka:' + predmet.vrijemePocetka + ',vrijemeKraja:' + predmet.vrijemeKraja;
                        fs.appendFile(__dirname + '/static/raspored.csv', line, function (err) {
                            if (err) {
                                res.status(404)
                                res.send("Problem pri upisu u datoteku!")
                                res.end()
                            }
                            console.log('Sve oke, ubaceno u datoteku.')
                            res.status(200)
                            res.send('Aktivnost uspjesno dodana u datoteku.')
                            res.end()
                        });
                    } else {
                        console.log("Greska pri upisu, aktivnost vec postoji ili je termin zauzet!")
                        res.status(300)
                        res.send('Greska: Aktivnost sa ovim nazivom se vec nalazi u datoteci ili se preklapa sa nekom drugom.\nNemoguce izvrsiti dodavanje aktivnosti.')
                        res.end()
                    }
                }
            });
        } else {
            res.status(400)
            res.send('Prosljedjeni nevalidni parametri!')
            res.end()
        }
    })


})

app.get('/v2/predmet', async (req, res) => {
    try {
        const predmeti = await Predmet.findAll()
        res.send(predmeti)
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.post('/v2/predmet', async (req, res) => {
    const predmetNaziv = req.body.naziv;
    try {
        await Predmet.create({
            naziv: predmetNaziv
        })
        res.send("Predmet kreiran").end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.put('/v2/predmet/:id', async (req, res) => {
    try {
        const predmetId = req.params.id
        const predmetNaziv = req.body.naziv
        await Predmet.update({ naziv: predmetNaziv }, {
            where: {
                id: predmetId,
            }
        })
        res.send('Predmet izmjenjen').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.delete('/v2/predmet/:id', async (req, res) => {
    try {
        const predmetId = req.params.id;
        await Predmet.destroy({
            where: {
                id: predmetId,
            }
        })
        res.send('Predmet izbrisan').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.get('/v2/dan', async (req, res) => {
    try {
        const dani = await Dan.findAll()
        res.send(dani)
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.post('/v2/dan', async (req, res) => {
    const danNaziv = req.body.naziv;
    try {
        await Dan.create({
            naziv: danNaziv
        })
        res.send("Dan kreiran").end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.put('/v2/dan/:id', async (req, res) => {
    try {
        const danId = req.params.id
        const danNaziv = req.body.naziv
        await Dan.update({ naziv: danNaziv }, {
            where: {
                id: danId,
            }
        })
        res.send('Dan izmjenjen').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.delete('/v2/dan/:id', async (req, res) => {
    try {
        const danId = req.params.id;
        await Dan.destroy({
            where: {
                id: danId,
            }
        })
        res.send('Dan izbrisan').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.get('/v2/tip', async (req, res) => {
    try {
        const tipovi = await Tip.findAll()
        res.send(tipovi)
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.post('/v2/tip', async (req, res) => {
    try {
        await Tip.create({
            ...req.body
        })
        res.send("Tip kreiran").end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.put('/v2/tip/:id', async (req, res) => {
    try {
        const tipId = req.params.id;
        await Tip.update({ ...req.body }, {
            where: {
                id: tipId,
            }
        })
        res.send('Tip izmjenjen').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.delete('/v2/tip/:id', async (req, res) => {
    try {
        const tipId = req.params.id;
        await Tip.destroy({
            where: {
                id: tipId,
            }
        })
        res.send('Tip izbrisan').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.get('/v2/grupa', async (req, res) => {
    try {
        const grupe = await Grupa.findAll()
        res.send(grupe)
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.post('/v2/grupa', async (req, res) => {
    try {
        await Grupa.create({
            ...req.body
        })
        res.send("Grupa kreirana").end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.put('/v2/grupa/:id', async (req, res) => {
    try {
        const grupaId = req.params.id;
        await Grupa.update({ ...req.body }, {
            where: {
                id: grupaId,
            }
        })
        res.send('Grupa izmjenjena').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.delete('/v2/grupa/:id', async (req, res) => {
    try {
        const grupaId = req.params.id;
        await Grupa.destroy({
            where: {
                id: grupaId,
            }
        })
        res.send('Grupa izbrisana').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.get('/v2/student', async (req, res) => {
    try {
        const studenti = await Student.findAll();
        res.send(studenti).end();
    } catch (error) {
        res.send(error).end();
    }
})

app.post('/v2/student', async (req, res) => {
    console.log(req.body);
    try {
        await Student.create({ ...req.body })
        res.send('Student kreiran').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

async function f(user, grupa, poruke) {
    try {
        await Student.create({ ime: user.ime, index: user.index }).then(student => StudentGrupa.create({ studentId: student.id, grupaId: grupa }))
        poruke.push(`Student ${user.ime} sa indeksom ${user.index} ubacen u bazu.`)
    } catch (error) {
        const upisani = await Student.findOne({ where: { index: user.index } })
        poruke.push(`Student ${user.ime} nije kreiran jer postoji student ${upisani.ime} sa istim indexom ${upisani.index}`)
    }
    return Promise.resolve()
}

app.post('/v2/studenti', async (req, res) => {
    const CSVToJSON = require('csvtojson');
    let poruke = [];
    CSVToJSON().fromString(req.body.data)
        .then(studenti => {
            Promise.all(studenti.map(student => f(student, req.body.grupa, poruke))).then(() => {
                res.send(JSON.stringify(poruke)).end()
            })
        })
})

app.put('/v2/student/:id', async (req, res) => {
    try {
        const studentId = req.params.id
        await Student.update(
            { ...req.body },
            {
                where: {
                    id: studentId,
                },
            }
        )
        res.send('Student promjenjen').end()
    } catch (error) {
        res.send(error).end()
    }
})

app.delete('/v2/student/:id', async (req, res) => {
    try {
        const studentId = req.params.id
        await Student.destroy({
            where: {
                id: studentId,
            },
        })
        res.send('Student obrisan').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.get('/v2/aktivnost', async (req, res) => {
    try {
        const aktivnosti = await Aktivnost.findAll()
        res.send(aktivnosti).end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.post('/v2/aktivnost', async (req, res) => {
    try {
        await Aktivnost.create({ ...req.body })
        res.send('Aktivnost kreirana').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.put('/v2/aktivnost/:id', async (req, res) => {
    try {
        await Aktivnost.update(
            { ...req.body },
            {
                where: {
                    id: req.params.id,
                },
            }
        )
        res.send('Aktivnost promjenjena').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.delete('/v2/aktivnost/:id', async (req, res) => {
    try {
        await Aktivnost.destroy({
            where: {
                id: req.params.id,
            },
        });
        res.send('Aktivnost obrisana').end()
    } catch (error) {
        res.status(500).send(error).end()
    }
})

app.listen(8080);