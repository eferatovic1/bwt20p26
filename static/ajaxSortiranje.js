function ucitajSortirano(dan, atribut, callback) {
    const http = new XMLHttpRequest();
    if (dan == null) dan = '';
    http.open('GET', `/v1/raspored?dan=${dan}&sort=${atribut}`);
    http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            callback(JSON.parse(this.responseText), null)
        }
        else if (this.readyState == 4 && this.status != 200) {
            callback(null, this.responseText)
        }
    }
    http.send();
}
