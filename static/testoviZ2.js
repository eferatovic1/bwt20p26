let assert = chai.assert;
describe('Raspored', function() {
    describe('dajTrenutnuAktivnost()', function() {
        it('Treba vratiti "Trenutno nema aktivnosti" jer grupa2 nema u datom vremenu nikakvih aktivnosti', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajTrenutnuAktivnost("16-11-2020T01:00:00", 'grupa2');
            assert.equal('Trenutno nema aktivnosti', x);
        });

        it('Treba vratiti "BWT-grupa2" jer grupa2 trenutno ima vjezbe', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajTrenutnuAktivnost("16-11-2020T13:30:00", 'grupa2');
            assert.equal('BWT-grupa2 90min', x);
        });

        it('Treba vratiti "BWT-grupa2" jer grupa2 trenutno ima vjezbe iako su na kraju', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajTrenutnuAktivnost("16-11-2020T15:00:00", 'grupa2');
            assert.equal('BWT-grupa2 0min', x);
        });

        it('Treba vratiti "Trenutno nema aktivnosti" jer grupa2 ima vjezbe ali vrsimo pretragu za grupu1', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajTrenutnuAktivnost("16-11-2020T14:00:00", 'grupa1');
            assert.equal('Trenutno nema aktivnosti', x);
        });

        it('Treba vratiti "BWT-grupa2 60min" jer grupa2 trenutno ima aktivnost', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajTrenutnuAktivnost("16-11-2020T14:00:00", 'grupa2');
            assert.equal('BWT-grupa2 60min', x);
        });
    });

    describe('dajPrethodnuAktivnost()', function() {
        it('Treba vratiti "BWT-grupa1" iako je prva prethodna grupa2, mi trazimo grupu1', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajPrethodnuAktivnost("16-11-2020T16:00:00", 'grupa1');
            assert.equal('BWT-grupa1', x);
        });
        it('Treba vratiti "MUR2" jer je to prethodna aktivnost', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajPrethodnuAktivnost("16-11-2020T08:00:00", 'grupa1');
            assert.equal('MUR2', x);
        });
    });

    describe('dajSljedecuAktivnost()', function() {
        it('Treba vratiti "FWT-grupa2 60min" iako je sljedeca aktivnost grupe 1 mi trazimo grupu2', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajSljedecuAktivnost("18-11-2020T10:00:00", 'grupa2');
            assert.equal('FWT-grupa2 60min', x);
        });
        it('Treba vratiti "Nastava je gotova za danas" jer nema vise aktivnosti obzirom da je kraj sedmice', function() {
            var rasporedObjekat = new Raspored(raspored);
            var x = rasporedObjekat.dajSljedecuAktivnost("13-11-2020T14:00:00", 'grupa2');
            assert.equal('Nastava je gotova za danas', x);
        });
    });
});