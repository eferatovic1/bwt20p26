
let naziv, aktivnost, dan, vrijemePocetka = null, vrijemeKraja = null;

function pretvoriVrijemeNazad(t) {
    let vrijeme = "";
    if (Number.isInteger(t)) {
        if (t < 10) {
            vrijeme += `0${t}:00`
        } else vrijeme += `${t}:00`
    } else {
        if (t < 10) {
            vrijeme += `0${t - 0.5}:30`
        } else vrijeme += `${t - 0.5}:30`
    }
    return vrijeme;
}

async function ispisi() {
    let tbl = '';
    let predmeti, tipovi, dani;
    let ucitajPredmete = await fetch('http://localhost:8080/v2/predmet');
    if (ucitajPredmete.ok) {
        predmeti = await ucitajPredmete.json();
    } else {
        alert("HTTP-Error: " + ucitajPredmete.status);
    }
    let ucitajTipove = await fetch('http://localhost:8080/v2/tip');
    if (ucitajTipove.ok) {
        tipovi = await ucitajTipove.json();
    } else {
        alert("HTTP-Error: " + ucitajTipove.status);
    }
    let ucitajDane = await fetch('http://localhost:8080/v2/dan');
    if (ucitajDane.ok) {
        dani = await ucitajDane.json();
    } else {
        alert("HTTP-Error: " + ucitajDane.status);
    }
    let ucitajAktivnosti = await fetch('http://localhost:8080/v2/aktivnost');
    if (ucitajAktivnosti.ok) {
        let ispisPredmet, ispisTip, ispisDan;
        let aktivnosti = await ucitajAktivnosti.json();
        for (a of aktivnosti) {
            pretvoriVrijemeNazad(a.pocetak)
            for (p of predmeti) {
                if (a.predmetId == p.id) ispisPredmet = p;
            }
            for (d of dani) {
                if (a.predmetId == d.id) ispisDan = d;
            }
            for (t of tipovi) {
                if (a.predmetId == t.id) ispisTip = t;
            }
        }
        for (let i = 0; i < aktivnosti.length; i++) {
            const aktivnost = aktivnosti[i];
            tbl += `<tr>
            <td>${aktivnost.naziv}</td>
            <td>${ispisPredmet.naziv}</td>
            <td>${ispisTip.naziv}</td>
            <td>${ispisDan.naziv}</td>
            <td>${pretvoriVrijemeNazad(aktivnost.pocetak)}</td>
            <td>${pretvoriVrijemeNazad(aktivnost.kraj)}</td>
            </tr>`;
        }
    } else {
        alert("HTTP-Error: " + ucitajAktivnosti.status);
    }
    document.getElementById('tabelaAktivnosti').innerHTML = tbl;
}

function pretvoriVrijeme(t) {
    vals = t.split(':')
    let hours = +(vals[0]);
    let minutes = +(vals[1]);
    if (minutes == 30) minutes = 0.5;
    return hours + minutes
}

function upisi(event) {
    event.preventDefault();
    let naziv = document.getElementById("unosNaziv").value
    let predmet = document.getElementById("unosPredmet").value
    let tip = document.getElementById("unosTip").value
    let dan = document.getElementById("unosDan").value
    let vrijemePocetka = document.getElementById("vrijemePocetka").value;
    let vrijemeKraja = document.getElementById("vrijemeKraja").value;
    let pocetak = pretvoriVrijeme(vrijemePocetka);
    let kraj = pretvoriVrijeme(vrijemeKraja);
    data = {

    }
    fetch("http://localhost:8080/v2/aktivnost", {
        method: "POST",
        body: JSON.stringify({
            naziv: naziv,
            pocetak: pocetak,
            kraj: kraj,
            predmetId: predmet,
            danId: dan,
            tipId: tip
        }),
        headers: {
            "Content-type": "application/json"
        }
    })
}

function checkForm() {
    let naziv = document.getElementById("unosNaziv").value
    let predmet = document.getElementById("unosPredmet").value
    let tip = document.getElementById("unosTip").value
    let dan = document.getElementById("unosDan").value
    let vrijemePocetka = document.getElementById("vrijemePocetka").value;
    let vrijemeKraja = document.getElementById("vrijemeKraja").value;
    let a = naziv && predmet && tip && dan && vrijemeKraja && vrijemePocetka;
    let pocpom = new Date(2020, 2, 2, vrijemePocetka.split(':')[0], vrijemePocetka.split(':')[1], 0, 0);
    let krajpom = new Date(2020, 2, 2, vrijemeKraja.split(':')[0], vrijemeKraja.split(':')[1], 0, 0);
    if (pocpom.getTime() >= krajpom.getTime()) {
        a = false;
        alert("Vrijeme pocetka mora biti prije vremena kraja aktivnosti!");
    }
    if (a) document.getElementById("submitButton").disabled = false;
    else {
        alert("Sva polja moraju biti popunjena!");
    }
}

async function ucitajPodatkeDB() {
    let ucitajPredmete = await fetch('http://localhost:8080/v2/predmet');
    if (ucitajPredmete.ok) {
        let predmeti = await ucitajPredmete.json();
        var select = document.getElementById('unosPredmet');
        for (let i = 0; i < predmeti.length; i++) {
            const predmet = predmeti[i];
            var opt = document.createElement("option");
            opt.value = predmet.id;
            opt.innerHTML = predmet.naziv;
            select.appendChild(opt);
        }
    } else {
        alert("HTTP-Error: " + ucitajPredmete.status);
    }
    let ucitajTipove = await fetch('http://localhost:8080/v2/tip');
    if (ucitajTipove.ok) {
        let tipovi = await ucitajTipove.json();
        var select = document.getElementById('unosTip');
        for (let i = 0; i < tipovi.length; i++) {
            const tip = tipovi[i];
            var opt = document.createElement("option");
            opt.value = tip.id;
            opt.innerHTML = tip.naziv;
            select.appendChild(opt);
        }
    } else {
        alert("HTTP-Error: " + ucitajTipove.status);
    }
    let ucitajDane = await fetch('http://localhost:8080/v2/dan');
    if (ucitajDane.ok) {
        let dani = await ucitajDane.json();
        var select = document.getElementById('unosDan');
        for (let i = 0; i < dani.length; i++) {
            const dan = dani[i];
            var opt = document.createElement("option");
            opt.value = dan.id;
            opt.innerHTML = dan.naziv;
            select.appendChild(opt);
        }
    } else {
        alert("HTTP-Error: " + ucitajDane.status);
    }
}
