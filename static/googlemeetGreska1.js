class GoogleMeet {
    static dajZadnjePredavanje(string) {
        const domparser = new DOMParser();
        const doc = domparser.parseFromString(string, 'text/html');
        const listaWeeks = doc.getElementsByClassName("weeks");
        var predavanjaLinkovi = [];

        if(listaWeeks && listaWeeks.length > 0) {
            listaWeeks[0].childNodes.forEach(i => {
                var linkoviPredavanja = i.getElementsByTagName('a');

                if(linkoviPredavanja && linkoviPredavanja.length > 0) {
                    Array.from(linkoviPredavanja).forEach(link => {
                        if(link.innerHTML.includes('predavanj')) {
                            predavanjaLinkovi.push(link);
                        }
                    });
                }
            });
    
            if (predavanjaLinkovi.length === 0 || predavanjaLinkovi[predavanjaLinkovi.length-1] === undefined) {
                return "";
            } else {
                return predavanjaLinkovi[predavanjaLinkovi.length - 1];
            }
        } else {
            return null;
        }

    }

    static dajZadnjuVjezbu(string) {        
        const domparser = new DOMParser();
        const doc = domparser.parseFromString(string, 'text/html');
        const listaWeeks = doc.getElementsByClassName("weeks");
        var vjezbeLinkovi = [];

        if(listaWeeks && listaWeeks.length > 0) {
            listaWeeks[0].childNodes.forEach(i => {
                var linkoviVjezbe = i.getElementsByTagName('a');

                if(linkoviVjezbe && linkoviVjezbe.length > 0) {
                    Array.from(linkoviVjezbe).forEach(link => {
                        if(link.innerHTML.includes('vježb') && link.href.includes('meet.google.com')) {
                            vjezbeLinkovi.push(link);
                        }
                    });
                }
            });
    
            if (vjezbeLinkovi.length === 0 || vjezbeLinkovi[vjezbeLinkovi.length-1] === undefined) {
                return null;
            } else {
                return vjezbeLinkovi[vjezbeLinkovi.length - 1];
            }
        } else {
            return "Nema vjezbi";
        }
    }
}