let assert = chai.assert;
describe('Googlemeet', function () {
    describe('dajZadnjePredavanje()', function () {
        it('Treba prikazati null obzirom da nema predavanja', function () {
            var x=GoogleMeet.dajZadnjePredavanje(testprimjeri[3]);
            assert.equal(null,x);
        });
        it('Treba vratiti null jer je string prazan', function () {
            var x=GoogleMeet.dajZadnjePredavanje("");
            assert.equal(null,x);
        });
        it('Treba vratiti null iako postoje adekvatni liknkovi unutar njih se ne nalaze rijec predavanj', function () {
            var x=GoogleMeet.dajZadnjePredavanje(testprimjeri[0]);
            assert.equal(null,x);
        });
        it('Treba vratiti predavanje iz prve sedmice jer je to jedino predavanje', function () {
            var x=GoogleMeet.dajZadnjePredavanje(testprimjeri[1]);
            assert.equal('http://meet.google.com/uca-utbm-fsv',x.href.toString());
        });
        it('Treba vratiti null jer kod html nije validan', function () {
            var x=GoogleMeet.dajZadnjePredavanje(testprimjeri[4]);
            assert.equal(null,x);
        });
    });
    describe('dajZadnjuVjezbu()', function () {
        it('Treba prikazati null obzirom da nema vjezbi', function () {
            var x=GoogleMeet.dajZadnjuVjezbu(testprimjeri[0]);
            assert.equal(null,x);
        });
        it('Treba vratiti null jer je string prazan', function () {
            var x=GoogleMeet.dajZadnjuVjezbu("");
            assert.equal(null,x);
        });
        it('Treba vratiti url vjezbe iz posljednje sedmice kada svaka druga sedmica ima vježbu', function () {
            var x=GoogleMeet.dajZadnjuVjezbu(testprimjeri[5]);
            assert.equal('http://meet.google.com/vjp-agsh-cgw',x.href.toString());
        });
        it('Treba vratiti null iako postoje adekvatni liknkovi unutar njih se ne nalaze rijeci vjezb ili vježb', function () {
            var x=GoogleMeet.dajZadnjuVjezbu(testprimjeri[0]);
            assert.equal(null,x);
        });
        it('Treba vratiti vjezbu iz prve sedmice jer je to jedina vjezba', function () {
            var x=GoogleMeet.dajZadnjuVjezbu(testprimjeri[1]);
            assert.equal('http://meet.google.com/jcm-tkvb-djz',x.href.toString());
        });
        it('Treba vratiti null jer kod html nije validan', function () {
            var x=GoogleMeet.dajZadnjuVjezbu(testprimjeri[4]);
            assert.equal(null,x);
        });
    });
});

