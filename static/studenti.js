
async function ucitaj() {
    let response = await fetch('http://localhost:8080/v2/grupa');
    if (response.ok) {
        let grupe = await response.json();
        var select = document.getElementById('unosGrupa');
        for (let i = 0; i < grupe.length; i++) {
            const element = grupe[i];
            var opt = document.createElement("option");
            opt.value = element.id;
            opt.innerHTML = element.naziv;
            select.appendChild(opt);
        }
    } else {
        alert("HTTP-Error: " + response.status);
    }

}

function posaljiZahtjev(event) {
    event.preventDefault()
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('unosStudent').value = JSON.parse(this.responseText).join("\n");
        } else if (this.readyState == 4 && this.status != 200) {
            document.getElementById('unosStudent').value = JSON.parse(this.responseText).join('\n');
        }
    };
    let data = "ime,index,\n" + document.getElementById('unosStudent').value
    let grupa = document.getElementById('unosGrupa').value
    xhttp.open("POST", "/v2/studenti", true);
    xhttp.setRequestHeader("Content-Type", "application/json")
    xhttp.send(JSON.stringify({ data: data, grupa: grupa }));
}
