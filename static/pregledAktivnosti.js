function ispisi(data, error) {
    let tbl = '';
    for (element of data) {
        tbl += `<tr><td>${element.naziv}</td><td>${element.aktivnost}</td><td>${element.dan}</td><td>${element.pocetak}</td><td>${element.kraj}</td></tr>`;
    }
    document.getElementById('tabelaAktivnosti').innerHTML = tbl;
}
let sort = 0;
function nacrtaj(dan, aktivnost) {
    let pom = '';
    if (sort == 0) {
        pom = 'D' + aktivnost;
        sort = 1;
    } else {
        pom = 'A' + aktivnost;
        sort = 0;
    }
    ucitajSortirano(dan, pom, ispisi);
}