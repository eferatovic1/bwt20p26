class Raspored {
    raspored = [];
    dani = [
        "nedjelja",
        "ponedjeljak",
        "utorak",
        "srijeda",
        "cetvrtak",
        "petak",
        "subota"
    ];
    vrijemeRegex = /\d{2}-\d{2}-\d{4}T\d{2}:\d{2}:\d{2}/;

    constructor(csv) {
        let linije = csv.split("\n");

        linije.forEach(linija => {
            var polja = linija.split(",");
            if (polja.length > 0) {
                this.raspored.push({
                    naziv: polja[0],
                    aktivnost: polja[1],
                    dan: polja[2],
                    start: polja[3],
                    end: polja[4]
                });
            }
        });

        this.raspored.sort((a, b) => {
            if (this.dani.indexOf(a.dan) < this.dani.indexOf(b.dan)) {
                return -1;
            } else if (this.dani.indexOf(a.dan) > this.dani.indexOf(b.dan)) {
                return 1;
            } else {
                if (a.start < b.start) return -1;
                else return 1;
            }
        });
    }

    dajTrenutnoVrijeme(vrijemeString) {
        var trenutnoVrijeme = vrijemeString.split("-").join(":");
        trenutnoVrijeme = trenutnoVrijeme.split("T").join(":");
        trenutnoVrijeme = trenutnoVrijeme.split(":");
        return new Date(
            trenutnoVrijeme[2],
            trenutnoVrijeme[1] - 1,
            trenutnoVrijeme[0],
            trenutnoVrijeme[3],
            trenutnoVrijeme[4],
            trenutnoVrijeme[5]
        );
    }

    dajTrenutnuAktivnost(vrijeme, grupa) {
        if (!this.vrijemeRegex.test(vrijeme)) {
            return "Vrijeme nije u trazenom formatu";
        }

        if (grupa == "") grupa = undefined;

        var vrijemeDoKraja;
        var predmetUToku;
        var trenutnoVrijeme = this.dajTrenutnoVrijeme(vrijeme);

        for (let i = 0; i < this.raspored.length; i++) {
            let predmet = this.raspored[i];
            if (
                this.dani.indexOf(predmet["dan"]) == trenutnoVrijeme.getDay() &&
                (predmet["naziv"].includes(grupa) ||
                    predmet["aktivnost"] == "predavanje")
            ) {
                var start = predmet["start"].split(":");
                var startVrijeme = new Date();
                startVrijeme.setFullYear(trenutnoVrijeme.getFullYear(), trenutnoVrijeme.getMonth(), trenutnoVrijeme.getDate());
                startVrijeme.setHours(start[0], start[1], 0, 0);

                var end = predmet["end"].split(":");
                var endVrijeme = new Date();
                endVrijeme.setFullYear(trenutnoVrijeme.getFullYear(), trenutnoVrijeme.getMonth(), trenutnoVrijeme.getDate());
                endVrijeme.setHours(end[0], end[1], 0, 0);

                if (startVrijeme <= trenutnoVrijeme && endVrijeme >= trenutnoVrijeme) {
                    predmetUToku = predmet["naziv"];
                    vrijemeDoKraja = Math.round(
                        (endVrijeme - trenutnoVrijeme) / 60 / 1000
                    );
                    break;
                }
            }
        }

        if (predmetUToku) return predmetUToku + " " + vrijemeDoKraja + "min";
        else return "Trenutno nema aktivnosti";
    }

    dajSljedecuAktivnost(vrijeme, grupa) {
        if (!this.vrijemeRegex.test(vrijeme)) {
            return "Vrijeme nije u trazenom formatu";
        }

        if (grupa == "") grupa = undefined;

        var vrijemeDoPocetka;
        var iduciPredmet;
        var trenutnoVrijeme = this.dajTrenutnoVrijeme(vrijeme);

        for (let i = 0; i < this.raspored.length; i++) {
            let predmet = this.raspored[i];
            if (
                this.dani.indexOf(predmet["dan"]) == trenutnoVrijeme.getDay() &&
                (predmet["naziv"].includes(grupa) ||
                    predmet["aktivnost"] == "predavanje")
            ) {
                var start = predmet["start"].split(":");
                var startVrijeme = new Date();
                startVrijeme.setFullYear(trenutnoVrijeme.getFullYear(), trenutnoVrijeme.getMonth(), trenutnoVrijeme.getDate());
                startVrijeme.setHours(start[0], start[1], 0, 0);

                var end = predmet["end"].split(":");
                var endVrijeme = new Date();
                endVrijeme.setFullYear(trenutnoVrijeme.getFullYear(), trenutnoVrijeme.getMonth(), trenutnoVrijeme.getDate());
                endVrijeme.setHours(end[0], end[1], 0, 0);


                if (startVrijeme >= trenutnoVrijeme && endVrijeme >= trenutnoVrijeme) {
                    let vrijemeDoPocetkaTemp = Math.round(
                        (startVrijeme - trenutnoVrijeme) / 60 / 1000
                    );
                    if (!vrijemeDoPocetka || vrijemeDoPocetkaTemp < vrijemeDoPocetka) {
                        iduciPredmet = predmet["naziv"];
                        vrijemeDoPocetka = Math.round(
                            (startVrijeme - trenutnoVrijeme) / 60 / 1000
                        );
                        break;
                    }
                }
            }
        }

        if (iduciPredmet) return iduciPredmet + " " + vrijemeDoPocetka + "min";
        else return "Nastava je gotova za danas";
    }

    dajPrethodnuAktivnost(vrijeme, grupa) {
        if (!this.vrijemeRegex.test(vrijeme)) {
            return "Vrijeme nije u trazenom formatu";
        }

        if (grupa == "") grupa = undefined;

        var prethodniPredmet;
        var trenutnoVrijeme = this.dajTrenutnoVrijeme(vrijeme);

        for (let i = 0; i < this.raspored.length; i++) {
            let predmet = this.raspored[i];

            if (this.dani.indexOf(predmet["dan"]) == trenutnoVrijeme.getDay() &&
                (predmet["naziv"].includes(grupa) ||
                    predmet["aktivnost"] == "predavanje")
            ) {
                var start = predmet["start"].split(":");
                var startVrijeme = new Date();
                startVrijeme.setFullYear(trenutnoVrijeme.getFullYear(), trenutnoVrijeme.getMonth(), trenutnoVrijeme.getDate());
                startVrijeme.setHours(start[0], start[1], 0, 0);

                var end = predmet["end"].split(":");
                var endVrijeme = new Date();
                endVrijeme.setFullYear(trenutnoVrijeme.getFullYear(), trenutnoVrijeme.getMonth(), trenutnoVrijeme.getDate());
                endVrijeme.setHours(end[0], end[1], 0, 0);

                if (startVrijeme <= trenutnoVrijeme && endVrijeme <= trenutnoVrijeme) {
                    prethodniPredmet = predmet["naziv"];
                }
            }
        }

        if (prethodniPredmet) {
            return prethodniPredmet;
        } else {
            while (prethodniPredmet == undefined) {
                trenutnoVrijeme.setDate(trenutnoVrijeme.getDate() - 1);

                var predmetiPrethodniDan = this.raspored.filter(
                    predmet => this.dani.indexOf(predmet.dan) == trenutnoVrijeme.getDay()
                );
                if (predmetiPrethodniDan.length > 0) {
                    prethodniPredmet =
                        predmetiPrethodniDan[predmetiPrethodniDan.length - 1];
                }
            }

            if (prethodniPredmet) return prethodniPredmet["naziv"];
        }
    }
}